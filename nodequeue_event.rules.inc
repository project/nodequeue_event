<?php
// $id$

/**
 * @file
 * 
 * Provides events to trigger when a nodequeue is udpated
 */

/**
 * Implementation of hook_rules_event_info().
 */
function nodequeue_event_rules_event_info() {
  $items = array();
  
  $queues = nodequeue_load_queues(array_keys(nodequeue_get_all_qids()));
  
  foreach ($queues as $queue) {
    $args = array(
      'node' => array(
        'type' => 'node',
        'label' => t('Node(s) acted on'),
      ),
      'oldnode' => array(
        'type' => 'node',
        'label' => t('Old node(s) acted on'),
      ),        
      'queue' => array(
        'type' => 'value',
        'label' => t('Queue'),
      ),
      'title' => array(
        'type' => 'value',
        'label' => t('Title'),
      ),
      'text' => array(
        'type' => 'value',
        'label' => t('Message to log'),
      ),
    );

    // Define single event for nodequeue
    $items['nodequeue_event_' . $queue->qid] = array(
      'label' => t('Nodequeue "@queue-title" updated.', array('@queue-title' => $queue->title)),
      'module' => 'Nodequeue',
      'arguments' => $args,
    );
  }
  
  return $items;
}
